#require(multifuse)
require(bfastSpatial)
require(bfast)
require(bayts2)
#require(strucchange)
#require(lubridate)


s1 <- brick("/media/DATA1/reich006/Data_processed/Bolivia/Sentinel1/S1_VL_MOS_VV.rtc_dB_mt55_res_stack_20141007_20160517_L.gri")

#load Landsat and assign projection utm 20S
landsat <- brick("/media/DATA1/reich006/Data_processed/Bolivia/Landsat/BOLIVIA2005_062016.gri")
crs(landsat) <- crs(s1)
extent(landsat) <- extent(s1)

# s1 get dates and pass (ascending and descending), and get asecending and descending raster brick
s1_pass <- substr(getZ(s1),12,12)
s1_date <- substr(getZ(s1),1,10)
s1A_date <- s1_date[s1_pass=="A"]
s1D_date <- s1_date[s1_pass=="D"]
s1A <- subset(s1,which(s1_pass %in% "A"))
s1D <- subset(s1,which(s1_pass %in% "D"))
# Landsat get dates
landsat_date <- as.Date(getSceneinfo(names(landsat))$date)
landsat_date


#plot valid landsat and s1 observations for overlapping period
#plot(countObs(subset(landsat,241:291)))
#plot(countObs(s1))

#reduce landsat raster brick to s1 observation period
landsat <- subset(landsat, 241:291, drop=FALSE)
landsat
landsat_date <- as.Date(getSceneinfo(names(landsat))$date)
landsat_date


####################################
#Create forest from shape and load  #
#####################################
fmask <- raster("/media/DATA1/reich006/Data_processed/Bolivia/shape/AllChangeBolivia20150925_utm20s.tif")
fmask[fmask >= 0]<-1
fmask
plot(fmask)

#mask s1 data with forest mask
s1m <- s1
landsatm <- landsat
fmask <- crop(fmask,extent(s1))
extent(fmask) <- extent(s1)


###################################################
#Sentinel-1: apply forest mask and deseasonalize  #
###################################################

s1m[!is.na(fmask)] <- NA
names(s1m) <- names(s1)
writeRaster(s1m,filename=paste("/media/DATA1/reich006/Data_processed/Bolivia/Sentinel1/S1_VL_MOS_VV.rtc_dB_mt55_res_stack_20141007_20160517_L_masked",sep=""),datatype="FLT4S",overwrite=TRUE)
#s1m <- brick("/media/DATA1/reich006/Data_processed/Bolivia/Sentinel1/S1_VL_MOS_VV.rtc_dB_mt55_res_stack_20141007_20160517_L_masked")
s1n <- deseasonalizeRaster(s1m,p=0.95)
writeRaster(s1n,filename=paste("/media/DATA1/reich006/Data_processed/Bolivia/Sentinel1/S1_VL_MOS_VV.rtc_dB_mt55_res_stack_20141007_20160517_L_Deseasonalized",sep=""),datatype="FLT4S",overwrite=TRUE)


###################################################
#Landsat: apply forest mask and deseasonalize  #
###################################################

landsatm[fmask==1] <- NA
landsatm <- landsatm/10000
writeRaster(landsatm,filename=paste("/media/DATA1/reich006/Data_processed/Bolivia/Landsat/BOLIVIA2005_062016_masked",sep=""),datatype="FLT4S",overwrite=TRUE)
landsatn <- deseasonalizeRaster(landsatm,p=0.95,min=0,max=1)
writeRaster(landsatn,filename=paste("/media/DATA1/reich006/Data_processed/Bolivia/Landsat/BOLIVIA2005_062016_Deseasonalized",sep=""),datatype="FLT4S",overwrite=TRUE)

plot(landsatn,1)
