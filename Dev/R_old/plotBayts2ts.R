#' @title Plot original time series including indicated and detected changes (2 time series)

#' @description Plot original time series including indicated and detected changes. T.flagged and T.confirmed are plotted

#' @author Johannes Reiche (Wageningen University)

#' @param bayts bayts time series data frame
#' @param ylim_ts1 the y limits (x1, x2) for time series 1. The default value is NULL, indicating that the ylim are set to the min and max value of time series 1
#' @param ylim_ts2 the y limits (x1, x2) for time series 2. The default value is NULL, indicating that the ylim are set to the min and max value of time series 2
#' @param lab_ts1 a title for time series 1
#' @param lab_ts2 a title for time series 2
#' @param col_ts1 colour for time series 1. The default value is "black"
#' @param col_ts2 colour for time series 2. The default value is "blue" 

#' @examples  
#' #TBD

#' @export 

plotBayts2ts <- function(bayts, lab_ts1="time series 1",lab_ts2="time series 2",ylim_ts1=NULL,ylim_ts2=NULL,col_ts1="black",col_ts2="blue"){
  
  #get output variables
  change.flagged = index(bayts[min(which(bayts$Flag=="Change"))])
  change.flggaed.PChange = bayts[min(which(bayts$Flag=="Change"))]$PChange
  change.confirmed = index(bayts[max(which(bayts$Flag=="Change"))])
  change.confirmed.PChange = bayts[max(which(bayts$Flag=="Change"))]$PChange
  flag = index(bayts[min(which(bayts$Flag=="Flag"))])
  oldflag = index(bayts[which(bayts$Flag=="oldFlag")])
  vchange = na.omit(bayts$Flag[which(bayts$Flag=="Change")])
  vflag = na.omit(bayts$Flag[which(bayts$Flag=="Flag")])
  bayts[min(which(bayts$Flag=="Change"))]$PChange

  #increase by 1 day in case of duplicated dates
  index(bayts)[which(as.character(duplicated(as.character(ts_to_Date_leapyears(index(bayts$ts1)))))==TRUE)]<-index(bayts)[which(as.character(duplicated(as.character(ts_to_Date_leapyears(index(bayts$ts1)))))==TRUE)]+0.001
  #get bfastts ts1 and ts2
  ts1 <- bfastts(as.double(bayts$ts1),ts_to_Date_leapyears(index(bayts$ts1)),type="irregular")
  if (ncol(bayts)>4){
    ts2 <- bfastts(as.double(bayts$ts2),ts_to_Date_leapyears(index(bayts$ts2)),type="irregular")
  } else {
    ts2 <- ts1
    ts2[] <- NA
  }
  
  
  #plot TS
  #if no xobs available
  if(all(is.na(ts1))==TRUE){
    plot1ts(ts2,lab=lab_ts2,ylim=ylim_ts2,col=col_ts2)
    #if no yobs available
  } else if(all(is.na(ts2))==TRUE){
    plot1ts(ts1,lab=lab_ts1,ylim=ylim_ts1,col=col_ts1)
    #xobs and yobs are available
  } else {
    plot2ts(ts1,ts2,lab_ts1=lab_ts1,lab_ts2=lab_ts2,ylim_ts1=ylim_ts1,ylim_ts2=ylim_ts2,col_ts1=col_ts1,col_ts2=col_ts2)
  }

  #plot title
  if(!is.na(flag)==TRUE){
    abline(v=flag,col='red',add=TRUE,lty='dashed')
    abline(v=oldflag,col='black',add=TRUE,lty='dashed') 
    title(paste("T.current=", round(max(index(bayts)),digits=3),
                "   [i=",as.double((length(vflag)-1)),", n=",as.double(length(vflag)),
                "; PChange= ",round(as.double(na.omit(bayts[length(index(bayts))]$PChange)),digits=3),"]",
                ";    T.flagged=", round(min(index(vflag)),digits=3)," (PChange=",round(as.double(change.flggaed.PChange),digits=3),")",sep=""),cex.main=1)
    
  } else if(!is.na(change.flagged)==TRUE){
    abline(v=change.flagged,col='red',lty='dashed')
    abline(v=change.confirmed,col='red')
    abline(v=oldflag,col='black',add=TRUE,lty='dashed')
    title(paste("T.current=", round(max(index(bayts)),digits=3),
                ";   T.flagged=", round(change.flagged,digits=3)," (PChange=",round(as.double(change.flggaed.PChange),digits=3),")",
                ", T.confirmed=",round(change.confirmed,digits=3)," (PChange=",round(as.double(change.confirmed.PChange),digits=3),")", sep=""),cex.main=1)
  } else {
    abline(v=oldflag,col='black',add=TRUE,lty='dashed')
    title(paste("Time: ", round(max(index(bayts)),digits=3)),cex.main=1)
  }

}