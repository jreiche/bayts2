#' @title Calculate probability for deforestation (old)
#' 
#' @description Calculate probability for deforestation
#' @author Johannes Reiche
#' @return bts time series data frame
#' @export 





calcPDef_old<- function(mts,minN=0,maxN=9999,maxt=9999,minPrDef=0.25,TPrDef_cloud=0,start=NULL,end=NULL,Nystop=NULL){
  names(mts) <- c("X","Y","PrF","PrNF","Flag","PrDef")
  #set cloud_stop:
  #cloud stop could also be equal to minPrDef
  #if i-1 was flagged as possible cloud then it is checked if PrDef < cloud_stop
  #if this is the case, it is stopped. This simulates a cloud.
  #for example. PrNF: 0.1, 0.9, 0.1 
  #initiate n (if n=N then stop)
  n<-0
  #set start & end time
  if(is.null(start)){st <- 1} else {st <- min(which(index(mts)>start))}
  if(is.null(end)){en <- length(index(mts))} else {en <- max(which(index(mts)<end))}
  
  print(mts)
  print(mts[st])
  print(mts[en])
  print(st)
  print(en)
  print("after")
  
  
  if(st!=1){
    mts$Flag[(st-1)] <- "0"
    #in case no historical data is available: attach a 50/50 dummy before the first observable
  } else {
    mts_fake <- mts[1]
    index(mts_fake) <- index(mts_fake)-0.1
    mts_fake$PrF <- 0.5
    mts_fake$PrNF <- 0.5
    mts <- rbind(mts_fake,mts)
    st <- st+1
    en <- en+1
    mts$Flag[(st-1)] <- "0"
  }
  
  
  #for number of observations in joint TS
  #en is < st if there are no observables in the monitoring period
  if(en >= st){
    for(i in st:en){
      print("in")
      #first observation    
      #if(i==st){
      #mts$Flag[i] <- "0"
      #remaining observations:
      #} else {
      #A) if possible Def was NOT flagged at i-1 (flag=0)
      #print(mts[(i-1)])
      #print(mts[(i)])
      #print(mts[(st)])
      # print(mts$Flag[(i-1)])
      
      if(mts$Flag[(i-1)] == "0" || mts$Flag[(i-1)] == "oldFlag") {
        #Case 1: if PrF >= PrNF
        if(mts$PrF[i]>=mts$PrNF[i]){
          mts$Flag[i]<-"0"
        }
        #Case 2: if PrF < PrNF -> change for Def at i
        if(mts$PrF[i]<mts$PrNF[i]){
          #calc Baysian Pr for Def
          #since it is the first Baysian cacl, we use preveous PrF as prior and current PrF as likelihood
          prior <- as.double(mts$PrNF[i-1])
          likelihood <- as.double(mts$PrNF[i])
          postieror <- (prior*likelihood)/((prior*likelihood)+((1-prior)*(1-likelihood)))
          mts$Flag[i]<- "1"
          mts$PrDef[i]<-postieror
          #set n=1
          n<-1
        } 
      } 
      #B) if possible Def was flagged at i-1 (flag=1)
      if(mts$Flag[(i-1)] == "1") {
        #prior = PrDef at i-1
        #Bayesian update is calculated
        prior <- as.double(mts$PrDef[i-1])
        likelihood <- as.double(mts$PrNF[i])
        postieror <- (prior*likelihood)/((prior*likelihood)+((1-prior)*(1-likelihood)))
        mts$PrDef[i]<-postieror
        #check if PrDef decreases below minPrDef threshold. This would mean no change
        if((as.double(mts$PrDef[i]))<TPrDef_cloud){
          mts$Flag[(i-n):i]<-0
          mts$Flag[(i-n)]<-"oldFlag"
          n<-0
          #PrDef >= minPrDef threshold
        } else {
          mts$Flag[i]<-"1"
          n<-n+1
        }
        
        #mts$Flag[i]<-"1"
        #n<-n+1
      } 
      #c) if Def was detected at i-1 (flag=Def) or post Deforestation (flag=postDef)
      if(mts$Flag[(i-1)] == "Def" || mts$Flag[(i-1)] == "postDef"){
        mts$Flag[i] <- "postDef"
      }
      #}
      
      ######### Part 2: check if flagged = Deforesation event according to parameter
      
      if(mts$Flag[(i)] == 1){
        stopF <- FALSE
        #check A: maximum obs time (maxt)
        tFlag <- index(mts[min(which(mts$Flag==1))])
        if(!is.na(tFlag)==TRUE){
          #ystop
          if(!is.null(Nystop)){
            #print(length(which(!is.na(mts$Y[which(mts$Flag=="1")])==TRUE)))
            print("N")
            print(n)
            print(minN)
            if(n>=minN){
              
              #if(mts[1]$Flag=="1"){}
              #mts<-nrt$mts
              
              
              #this should be double checked. 
              #if(length(which(mts$Y[which(!is.na(mts$Y[which(mts$Flag=="0")])==TRUE)]>minPrDef))>=Nystop){
              print(Nystop)
              if(length(which(!is.na(mts$Y[which(mts$Flag=="1")])==TRUE))>=Nystop){
                #check if prNF of last flagged PALSAR observation > minPrDef
                print("inside Nysopt1")
                #print(mts[which(index(mts) == max(index(na.omit(mts$Y[which(mts$Flag=="1")]))))]$PrNF)
                if(mts[which(index(mts) == max(index(na.omit(mts$Y[which(mts$Flag=="1")]))))]$PrNF>=0.5){
                  print("inside Nystop")
                  #print(length(which(!is.na(mts$Y[which(mts$Flag=="1")])==TRUE)))
                  #print(mts[which(index(mts) == max(index(mts$Y[which(mts$Flag=="1")])))]$PrNF)
                  #print(mts[which(index(mts) == index(na.omit(mts$Y[which(mts$Flag=="1")])))]$PrNF)
                  #print(as.double(mts$PrDef[i]))
                  print(i)
                  if((as.double(mts$PrDef[i]))>=0.5){
                    print("inside Dec")
                    mts$Flag[(i-n+1):i]<-"Def"
                    stopF <- TRUE
                    #n <- n+1
                 print("after dec")
                  }
                } 
                if(stopF==FALSE){
                if(mts[which(index(mts) == max(index(na.omit(mts$Y[which(mts$Flag=="1")]))))]$PrNF < (1-0.5)) {
                  print("inside Dec2")
                  mts$Flag[(i-n+1):i]<-0
                  mts$Flag[(i-n+1)]<-"oldFlag" 
                  n <- 0
                  stopF <- TRUE
                }
                }
              }
            }
          }
          print(stopF)
          if(stopF==FALSE){
            Bmaxt <- FALSE
            print(Bmaxt)
            print((index(mts[i])))
            print(length(index(mts)))
            print(index(mts[i])==length(index(mts)))
            if(i==length(index(mts))){
            #  Bmaxt <- TRUE
            #} else if((index(mts[i+1])-tFlag) >= maxt){Bmaxt <- TRUE}
            } else if((index(mts[i+1])-tFlag) >= maxt && index(mts[i+1]) <= index(mts[en])){Bmaxt <- TRUE}
            ## Following needs to be unmarked if maxt + next observation should be considered
            #if((index(mts[i])-tFlag) >= maxt){Bmaxt <- TRUE}
            print(Bmaxt)
            if(Bmaxt==TRUE){
              #check if n >= minN
              if(n >= minN){
                #if PrDef > minPrDef then Deforestation
                print("inside maxt")
                print(as.double(mts$PrDef[i]))
                if((as.double(mts$PrDef[i]))>=minPrDef){
                  mts$Flag[min(which(mts$Flag==1)):i]<-"Def"
                  stopF <- TRUE
                } else {
                  #mts$Flag[(i-n+1):i]<-0
                  #mts$Flag[(i-n+1)]<-"oldFlag" 
                  #n <- 0
                  #stopF <- TRUE
                }
              }
            }    
          }
          
          #if t at i - first flag > maxt threshold and n >= minN -> then stop
          
        }
      
        print(stopF)
        #check B: maximum number of observation (N)
        #if n==maxN then stop
        if(stopF==FALSE){
          if(n>=maxN){
            if((as.double(mts$PrDef[i]))>=minPrDef){
              mts$Flag[(i-maxN+1):i]<-"Def"
              #n <- n+1
              } else {
              mts$Flag[(i-n+1):i]<-0
              mts$Flag[(i-n+1)]<-"oldFlag" 
              n <- 0
              stopF <- TRUE
            }
          }
        }
        
      }
      
      
    }
  }
  names(mts) <- c("ts1","ts2","PF","PNF","Flag","PDef")
  return(mts)
}

#Date functions
#ts_to_Date_leapyears function
ts_to_Date_leapyears <- function(x)
  #bfastts does not consider leapyear!!
  #unique(1900 + as.POSIXlt(dates)$year + (yday365(dates) - 1)/365)
  #length(1900 + as.POSIXlt(dates)$year + (yday365(dates) - 1)/365)
  #duplicated(1900 + as.POSIXlt(dates)$year + (yday365(dates) - 1)/365)
  #find duplicates in ts with refere to 29th February (leapyear)
  #instead of 29th feb bfastts writes 1.march
{
  #if lapyera add 1
  yu<-function(yr){
    u <- 0
    if(yr %% 4 == 0) {u <- 1}
    return(u)
  }
  conv.frac.yr <- function(yr) as.Date((yr-1970)*(365 + yu(yr)) + round((yr-1970)/4) , origin="1970-01-01" ) 
  return(as.Date(sapply(x, conv.frac.yr), origin="1970-01-01") )
}
