################################################################
# Reproducible example
# Author: Johannes Reiche
# Date: 04.02.2014
##reich006: Adapted bfastmonitor to run it on spare ts with history period >= 2
##reich006: Change 1: line27: Check if at least 1 obs in history period, otherwise stop function and return NA
##reich006: Change 2: line79: Dynamic h-value (for h=0.25 at least 8 observations in historic period have to exist, h=0.5 for at least 4)
##reich006: Change 3: line90: check for minimum number of observations in history; >= 2 seem to be threshold that method provides change
################################################################

require(raster)
require(bayests)  ## this loads the bfast package
require(bfast)

#load single pixel data
load("Def_4_cell_3919523.RData")

#create bfastts
dd_hh_mt <- bfastts(hh_mt,hh_dates,type=c("irregular"))
dd_hv_mt <- bfastts(hv_mt,hv_dates,type=c("irregular"))
dd_P <- dd_hv_mt-dd_hh_mt
dd_L <- bfastts(ndvi_clean,ndvi_dates,type=c("irregular"))
############ How to use bayests functions
#get dates of TS obs
DL <- index(dd_L)[which(!is.na(dd_L))]
DP <- index(dd_P)[which(!is.na(dd_P))]

#create joint array and assing L and P to respective date
l <- length(DL)+length(DP)
l
index(dd_P)
#function for simulating NRT
#returns 2 ts at nrt position obs i (ts1 u ts2)
yu <- subset.2ts(dd_L,dd_P,l)
Ndd_L <-  yu[[1]]
Ndd_P <-  yu[[2]]
DL <- index(Ndd_L)[which(!is.na(Ndd_L))]
DP <- index(Ndd_L)[which(!is.na(Ndd_L))]

plot.2ts(Ndd_L,Ndd_P,lab_ts1="NDVI",lab_ts2="HVHH",points=TRUE)


##Define parameters for change detection
#Define distributions to calculate conditional probability for F and NF
yDistF <-  c(-4.95,0.69)
yDistNF <- c(-7.37,1.38)
yDistPDF <- c("normal","normal")
yDist <- c(yDistPDF,yDistF,yDistNF)
yDist
xDistF <-  c(1.64e+01,8.67e-01)
xDistF <-  c(0.839,0.07)
xDistNF <- c(0.56,0.155)
xDistPDF <- c("normal","normal")
xDist <- c(xDistPDF,xDistF,xDistNF)
xDist
#Min and max PF for block weighting function
minPF <- 0.1
maxPF <- 0.9

Dummy_Ndd_L <- Ndd_L
Dummy_Ndd_L[]<-NA
Dummy_Ndd_P <- Ndd_P
Dummy_Ndd_P[]<-NA

#Both time series
bayts <- create.bayts_2ts(Ndd_L,Ndd_P,xDist,yDist,c(minPF,maxPF))
bayts <- calc.PChange(bayts,chi_ts1=0.99,chi_ts2=0.75,start=2008,end=2010.75)
#get time of change flagged
get.t_change_flagged(bayts)
#get time of change confirmed
get.t_change_confirmed(bayts)
#plot bayts
plot.bayts(bayts) 
plot.bayts_PNF(bayts) 
